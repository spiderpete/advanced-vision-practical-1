This describes the first assignment for assessment on Advanced Vision. The main goal is to reliably detect and track a set of table tennis balls bouncing on a table in a set of video data. 

### **Task Background:**

At the URL: 

http://homepages.inf.ed.ac.uk/rbf/AVDATA/AV115DATA 

a tar file dataset1_1415.tar of 63 consecutive images (000000XX.jpg, XX=25..87) plus a background image bgframe.jpg can be found. The images show a set of 10 table tennis balls (2 pink, 2 white and 6 orange) bouncing on a plain table. The camera is essentially stationary viewing a stationary background. The images show the balls bouncing around until they bounce off of the table. 

The folder contains a file (balls_loc.mat) listing the ‘ground-truth’ position of the ball centres. These are recorded in a data structure: 

* 	new balls(ball id).frame numbers(sample id) 
* 	new balls(ball id).row of centers(sample id) 
* 	new balls(ball id).col of centers(sample id) 

where ball id=1..10 and sample id indexes the observations of that ball. For a given ball id, the frames where the ball is seen are: new balls(ball id).frame numbers. The number of frames where ball id is seen is numel(new balls(ball id).frame numbers). So, for example, the second frame of the third ball is: new balls(3).frame numbers(2). The list of columns of the ball centre is given by: new balls(ball id).col of centers(sample id) (similarly for the rows). The colours of the balls 1..10 are: {‘white 1’, ‘white 2’, ‘pink 1’, ‘pink 2’, ‘orange 1’, ‘orange 2’, ‘orange 3’, ‘orange 4’, ‘orange 5’, ‘orange 6’}. 

The tar file also contains a function drawpos.m that illustrates the use of the data structure, by drawing the ball ground truth positions on the image for each frame, with a brief pause in between. 

The background image (bgframe.jpg) and a typical frame (00000062.jpg) with the balls detected and circlec are here. The balls enter from the top. 

The overall task for this assignment is to detect and track the balls, even though they are bouncing (and may collide). To do this we wrote a set of programs that can: 

1. Detect the moving balls, 
1. Compute a trajectory for each ball through all frames (taking account of collisions), and 
1. Evaluate the correctness of the detections and tracking against the ground truth dataset. 

Each of these is described in more detail below. 

### **Ball Detector:** ###

The image data is a set of RGB colour images and the camera is fixed. There is a constant largely featureless background image and the lighting is mostly constant. There are some shadows from objects in the room and the balls also create new moving shadows. We are able to easily detect the marbles using a background subtraction method. We find that using normalised RGB helps to remove the effect of the shadows, but remember that there are also 2 white balls, so we need to use both the normalised and unnormalised images, and then apply some reasoning based on what we know about the balls. 

We use operations like open/close or dilate/erode to clean up the image. We also use the largest connected components. 

We label each ball uniquely when it first appears and try to keep the same numerical label throughout. 

We represent each ball by its centre of mass and radius. 

### **Tracking:** 

We determine the correspondence between the balls in each image. Some balls will not move much between consecutive frames, but others will. There are up to 10 balls in each image. There are several collisions. 

The information that can most easily be exploited is: 

1. The RGB colour distribution of each ball is nearly constant. We represent each ball by a 2D histogram of the r/g components from normalised RGB values. Comparing histograms could use the Bhattacharyya distance, 
1. All balls appear and disappear at the edges of the image, not in the middle, so balls in the middle will have to match something from the previous and next frames. 
1. There are few collisions. 
1. There are 2 white, 2 pink and 6 orange balls. 

The Condensation tracker discussed in the lecture video is a good way to solve the tracking problem, because it can keep different hypotheses for each of the balls, which is very helpful when considering potentially ambiguous tracking correspondence hypotheses. While one could consider a combined state vector for all 10 balls, it is simpler to have one set of state vectors (and thus one condensation tracker) for each ball being tracked. A new tracker is started for each new detected ball and stopped when the ball leaves the field of view permanently. The ball radius helpd resolve ambiguities. 

We also need the Condensation tracker to cope with collisions. This requires an extension of the state transition diagram to include the out-of-scene and collision events and estimate new transition probabilities. 

On the other hand, a Kalman filter is not necessary for this problem. 

### **Evaluation:** ###

The ground truth file shows the ball centres as found by hand, for each ball. We compare our estimated ball centre (using the centre of mass of the detected pixels) with the ground truth centre for each ball and each frame (273 comparisons in total). We report the number of balls that were detected within 10 pixels of any ground truth centre. We report the mean distance between the ground truth and estimated centres, for all balls within the 10 pixel distance threshold. We report the number of false ball detections. 

As part of the assignment, we plot your ball detections on top of each frame by drawing a circle of the estimated radius (pArea/π) around the detected position. We include a few of these images in our report. 

We also want to evaluate the quality of the tracker. 

We compute the percentage of the instances where our tracker correctly pairs the balls between consecutive frames. Ie. suppose ball a is found at positions ~pt and ~pt+1 in frames t and t + 1. If our program detected these 2 balls, then a correct trajectory pairing means that our program should have assigned them the same trajectory identifier. If they don’t have the same identifier, then this is a tracking error. 

We report the number of correct and erroneous pairings. We also report the percentage of ball detections that belong to trajectories with only 1 detection. 

The final tracking evaluation is done by hand: We draw an image of all ground-truth trajectories on top of an image frame, where each trajectory has a different colour (reusing matlab colours as necessary). Then we draw a similar image showing each of your trajectories on another image, again with different colours for each trajectory. We break the trajectory when the ball leaves the image. Finally, we report the number of tracked trajectories for each of the 10 balls. We include the images in our report.
