function foremn = bg_Substraction(current_frame, background)
    
    %Performing errosion
    %foremn = bwmorph(fore, 'erode', 2);

    %Perform filling
    %foremnf = imfill(foremn, 'holes');
    
    threshold = 17;

    diffR = abs(current_frame(:,:,1) - background(:,:,1));
    diffG = abs(current_frame(:,:,2) - background(:,:,2));
    diffB = abs(current_frame(:,:,3) - background(:,:,3));
    
    fore = diffR > threshold | diffG > threshold | diffB > threshold;
    
    % Opening
    foremn = bwareaopen(fore, 150, 8);
