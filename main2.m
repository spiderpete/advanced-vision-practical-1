load balls_loc.mat

% Anotation/Tagging
num_balls = size(new_balls,2);
present = zeros(1, num_balls);
limits  = zeros(1, num_balls);
nextid  = ones(1, num_balls);
ball_name = {'white 1', 'white 2', 'pink 1', 'pink 2', 'orange 1', 'orange 2', 'orange 3', 'orange 4', 'orange 5', 'orange 6'};
file_name='/afs/inf.ed.ac.uk/user/s11/s1141453/Documents/Year4/AV/CW1/Images/';
file_format='.jpg';

% Retain background image
bg_file = [file_name 'bgframe.jpg'];
background = imread(bg_file);

n_background = normalisation(background);

history = zeros(size(n_background,1), size(n_background, 2), size(n_background, 3), 5);
% history(:,:,:,1) = n_background;
for i = 1:size(history, 4);
    %history(:,:,:,i) = 1.01 * history(:,:,:,i - 1);
    history(:,:,:,i) = n_background;
end

filename = [file_name sprintf('%08d', 25) file_format];
previous_frame = imread(filename);
prev_binary = zeros(size(previous_frame, 1), size(previous_frame, 2));
for i = 25:87
	filename = [file_name sprintf('%08d', i) file_format];
	current_frame=imread(filename);
    
    
    % Background Subtraction Model - Proprietary - Originals
    foremn = bg_Substraction(current_frame, background);
    
    % Background Subtraction Model - Proprietary - Normalised
    n_frame = normalisation(current_frame);
    
    n_back = normalisation(background);
    
    foremn2 = bg_Substraction(n_frame, n_back); % RGS
    
    % Background Subtraction Model - Matlab - Originals   
    foremn3 = imabsdiff(background, current_frame);
    
    % Background Subtraction Model - Matlab - Normalised    
    foremn4 = imabsdiff(n_back, n_frame);
    
    % Inter-Frame Background Subtraction Model - Matlab - Originals    
    %foremn5 = imabsdiff(previous_frame, current_frame);
    %previous_frame = current_frame;
    
    % External Function for the above
    current_binary = interFrameBG_Subtraction(current_frame, previous_frame, prev_binary);
    previous_frame = current_frame;
    prev_binary = current_binary;
    
    %difference = im2bw(foremn6, 0.06); % 0.06
    difference = bwareaopen(current_binary,250); % 250
	difference = imfill(difference, 'holes');
    
    object = bwmorph(difference, 'erode', 1); % 1
	object = ~object;
    
    % Get rid of noise in image
	labeled = bwlabel(object, 4);
    
    % Create image with only the edges around object
		edges = edge(labeled);
		
		% create RGB image with green edges
		edges2 = cat(3,zeros(size(edges, 1), size(edges, 2)), edges, zeros(size(edges, 1), size(edges, 2)));

		% 'Overlap' the RGB frame with the greep edges image to obtain contour around the ball
        %detectedObj = zeros(size(difference,1), size(difference, 2), 3);
		detectedObj = current_frame;
        
		for z = 1 : size(edges2,1)
			for j = 1 : size(edges2,2)
				if (edges2(z,j,2) ~= 0)
					detectedObj(z,j,1) = 0;
					detectedObj(z,j,2) = 255;
					detectedObj(z,j,3) = 0;
			    	end
			end
        end
        
    % Get each object in frame and it's index
    
    [labeledImage, numberOfObjects] = bwlabel(~object);

	% Get the properties of each object in the frame
	blobMeasurements = regionprops(labeledImage, 'Perimeter', 'FilledArea', 'Centroid');
	boundaries = bwboundaries(~object);
	perimeters = [blobMeasurements.Perimeter];
	filledAreas = [blobMeasurements.FilledArea];

	% Calculate circularities:
	circularities = perimeters .^2 ./ (4 * pi * filledAreas);
    
    foremn = bwmorph(foremn, 'clean');
    % erode -> dilate
    foremn = bwmorph(foremn, 'close');
    %foremn = imfill(foremn, 'holes');
    foremn = bwareaopen(foremn, 100);
    
    imshow(current_binary);
    title(i);
    hold on;
    
    % Go through each object in frame
		for blobNumber = 1 : numberOfObjects
			% Determine the shape
			if (circularities(blobNumber) < 1.3 && circularities(blobNumber) > 0.7 && perimeters(blobNumber) > 20)
				current_xy = blobMeasurements(blobNumber).Centroid;
                mySize = blobMeasurements(blobNumber).Perimeter / pi;
                plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);
			end
        end
    
    % Anotation/Tagging
	for j = 1:num_balls
		limits(j) = numel(new_balls(j).row_of_centers);
		if nextid(j) <= limits(j)
			if new_balls(j).frame_numbers(nextid(j)) == i
				text(new_balls(j).row_of_centers(nextid(j)), new_balls(j).col_of_centers(nextid(j)), ball_name{j}, 'Clipping', 'on', 'Color', 'cyan');
				plot(new_balls(j).row_of_centers(nextid(j)), new_balls(j).col_of_centers(nextid(j)), 'g+');
				nextid(j) = nextid(j) + 1;
			end
		end
    end
    
	pause(0.1);
    
    disp(i);
    
end

disp('We are doneeee!! YEEEEY');
