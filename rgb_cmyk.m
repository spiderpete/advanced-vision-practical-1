function [cmyk] = adaptive_bg_model(rgb)

r = rgb(:,:,1);
g = rgb(:,:,2);
b = rgb(:,:,3);

rp = r ./ 255;
gp = g ./ 255;
bp = b ./ 255;


%Calculate K (black) channel
k = zeros(size(r, 1), size(r, 2));

for i = 1 : size(r, 1)
    for j = 1 : size(r, 2)
        one = rp(i,j);
        two = gp(i,j);
        three = bp(i,j);
        k(i, j) = 1 - max(max(one, two), three);
    end
end

%Calculate C (cyan) channel
c = zeros(size(r, 1), size(r, 2));

c = 1 - rp;
c = double(c) - double(k);
c2 = 1 - k;

c = c ./ c2;

%Calculate M (magenta) channel
m = zeros(size(r, 1), size(r, 2));

m = 1 - gp;
m = double(m) - double(k);
m2 = 1 - k;

m = m ./ m2;

%Calculate Y (yellow) channel
y = zeros(size(r, 1), size(r, 2));

y = 1 - bp;
y = double(y) - double(k);
y2 = 1 - k;

y = m ./ y2;

cmyk = cat(4, c, m, y, k);