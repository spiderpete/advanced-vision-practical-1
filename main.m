load balls_loc.mat

% Anotation/Tagging
num_balls = size(new_balls,2);
present = zeros(1, num_balls);
limits  = zeros(1, num_balls);
nextid  = ones(1, num_balls);
ball_name = {'white 1', 'white 2', 'pink 1', 'pink 2', 'orange 1', 'orange 2', 'orange 3', 'orange 4', 'orange 5', 'orange 6'};
file_name='/afs/inf.ed.ac.uk/user/s11/s1141453/Documents/Year4/AV/CW1/Images/';
file_format='.jpg';

% Retain background image
bg_file = [file_name 'bgframe.jpg'];
background = imread(bg_file);

n_background = normalisation(background);

history = zeros(size(n_background,1), size(n_background, 2), size(n_background, 3), 5);
% history(:,:,:,1) = n_background;
for i = 1:size(history, 4);
    %history(:,:,:,i) = 1.01 * history(:,:,:,i - 1);
    history(:,:,:,i) = n_background;
end




for i = 25:87
	filename = [file_name sprintf('%08d', i) file_format];
	current_frame=imread(filename);
    
    foremn = bg_Substraction(current_frame, background);
    
    % Adaptive background model
    n_frame = normalisation(current_frame);
    
    [unsortedMedianR, foremn2, history] = adaptive_bg_model(n_frame, history);
    
    
	clc
    imshow(foremn);
	hold on
    
    % Anotation/Tagging
	for j = 1:num_balls
		limits(j) = numel(new_balls(j).row_of_centers);
		if nextid(j) <= limits(j)
			if new_balls(j).frame_numbers(nextid(j)) == i
				text(new_balls(j).row_of_centers(nextid(j)), new_balls(j).col_of_centers(nextid(j)), ball_name{j}, 'Clipping', 'on', 'Color', 'cyan');
				plot(new_balls(j).row_of_centers(nextid(j)), new_balls(j).col_of_centers(nextid(j)), 'g+');
				nextid(j) = nextid(j) + 1;
			end
		end
	end
	pause(0.5)    
    
    disp(i);
    
end

disp('We are doneeee!! YEEEEY');
