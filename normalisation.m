function n_frame = normalisation(current_frame)

    % Calculate normalised frame
    Im = im2double(current_frame); % convert to double precision
    imR = squeeze(Im(:,:,1));
    imG = squeeze(Im(:,:,2));
    imB = squeeze(Im(:,:,3));
    imRNorm = imR./(imR+imG+imB);
    imGNorm = imG./(imR+imG+imB);
    %imBNorm = imB./(imR+imG+imB);
    imS = (imR + imG + imB) / 3;
    n_frame = cat(3, imRNorm, imGNorm, imS);