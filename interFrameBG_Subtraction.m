function current_binary = interFrameBG_Subtraction(current_frame, prev_frame, prev_binary)
    threshold = 10;
        
    temp = imabsdiff(current_frame, prev_frame);
    
    % FG = 1; BG = 0
    mask = (temp(:,:,1) > threshold) | (temp(:,:,2) > threshold) | (temp(:,:,3) > threshold);    
    current_binary = xor(mask, prev_binary);
    
    
    current_binary = bwmorph(current_binary, 'clean');
    %current_binary = bwmorph(current_binary, 'erode'); % 1
    % erode -> dilate
    %current_binary = bwmorph(current_binary, 'close');
    current_binary = bwmorph(current_binary, 'fill');
    current_binary = bwareaopen(current_binary, 550);
    %current_binary = bwmorph(current_binary, 'remove');
    %current_binary = imfill(current_binary, 'holes');
    
    current_binary = (current_binary - prev_binary) > 0;
    
end