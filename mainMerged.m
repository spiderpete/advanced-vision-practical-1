load balls_loc.mat

% Anotation/Tagging
num_balls = size(new_balls,2);
present = zeros(1, num_balls);
limits  = zeros(1, num_balls);
nextid  = ones(1, num_balls);
ball_name = {'white 1', 'white 2', 'pink 1', 'pink 2', 'orange 1', 'orange 2', 'orange 3', 'orange 4', 'orange 5', 'orange 6'};
file_name='D:\Documents - Drive (D)\Studies\Year 4\Semester 2\Advanced Vision\CourseWork\Assignment 1\Images\';
file_format='.jpg';

% Retain background image
bg_file = [file_name 'bgframe.jpg'];
background = imread(bg_file);

n_background = normalisation(background);

history = zeros(size(n_background,1), size(n_background, 2), size(n_background, 3), 5);
% history(:,:,:,1) = n_background;
for i = 1:size(history, 4);
    %history(:,:,:,i) = 1.01 * history(:,:,:,i - 1);
    history(:,:,:,i) = n_background;
end


% Using repmat to store a structure for each frame
N = 100;
b = repmat(struct('structure', 1), N, 1);

filename = [file_name sprintf('%08d', 25) file_format];
previous_frame = imread(filename);
prev_binary = zeros(size(previous_frame, 1), size(previous_frame, 2));

prev_clearOutput = zeros(size(prev_binary, 1), size(prev_binary, 2));

for i = 25:87
    
    %Structure for ball tagging
    structure = struct('Colour', {}, 'Perimeter', {}, 'FilledArea', {}, 'Centroid', {}, 'Circularity', {}, 'PixelList', {}, 'Channel1', {}, 'Channel2', {}, 'Channel3', {});
    counter = 0;
    
	filename = [file_name sprintf('%08d', i) file_format];
	current_frame=imread(filename);
    
    
    % Background Subtraction Model - Proprietary - Originals
    foremn = bg_Substraction(current_frame, background);
    
    % Background Subtraction Model - Proprietary - Normalised
    n_frame = normalisation(current_frame);
    
    n_back = normalisation(background);
    
    foremn2 = bg_Substraction(n_frame, n_back); % RGS
    
    % Background Subtraction Model - Matlab - Originals   
    foremn3 = imabsdiff(background, current_frame);
    
    % Background Subtraction Model - Matlab - Normalised    
    foremn4 = imabsdiff(n_back, n_frame);
    
    % Inter-Frame Background Subtraction Model - Matlab - Originals    
    %foremn5 = imabsdiff(previous_frame, current_frame);
    %previous_frame = current_frame;
    
    % External Function for the above
    current_binary = interFrameBG_Subtraction(current_frame, previous_frame, prev_binary);
    %previous_frame = current_frame;
    %prev_binary = current_binary;
    
    difference = im2bw(foremn4, 0.06); % 0.06
    difference = bwareaopen(difference,250); % 250
	difference = imfill(difference, 'holes');
    
    object = bwmorph(difference, 'erode', 1); % 1
	object = ~object;
    
     % Get each object in frame and it's index
    [labeledImage, numberOfObjects] = bwlabel(~object);

	% Get the properties of each object in the frame
	blobMeasurements = regionprops(labeledImage, 'Perimeter', 'FilledArea', 'Centroid', 'PixelList');
    
	%boundaries = bwboundaries(~object);
	perimeters = [blobMeasurements.Perimeter];
	filledAreas = [blobMeasurements.FilledArea];

	% Calculate circularities:
	circularities = perimeters .^2 ./ (4 * pi * filledAreas);
    imshow(current_binary);
    
    %{
    % Get rid of noise in image
	labeled = bwlabel(object, 4);
    
    % Create image with only the edges around object
    edges = edge(labeled);

    % create RGB image with green edges
    edges2 = cat(3,zeros(size(edges, 1), size(edges, 2)), edges, zeros(size(edges, 1), size(edges, 2)));

    % 'Overlap' the RGB frame with the greep edges image to obtain contour around the ball
    %detectedObj = zeros(size(difference,1), size(difference, 2), 3);
    detectedObj = current_frame;

    for z = 1 : size(edges2,1)
        for j = 1 : size(edges2,2)
            if (edges2(z,j,2) ~= 0)
                detectedObj(z,j,1) = 0;
                detectedObj(z,j,2) = 255;
                detectedObj(z,j,3) = 0;
            end
        end
    end
    %}    
    
    
    % Image(Binary) filtering (mainly for foremn)
    foremn = bwmorph(foremn, 'clean');
    % erode -> dilate
    foremn = bwmorph(foremn, 'close');
    %foremn = imfill(foremn, 'holes');
    foremn = bwareaopen(foremn, 100);
    
    
    % Post-Processing for Interframe BG subtraction
    
    % Get each object in frame and it's index
    markedBlobs = bwconncomp(current_binary);
    stats = regionprops(markedBlobs, 'Area');
    bigBlobsIds = find([stats.Area] > 5000);
    markedBlobsMask = ismember(labelmatrix(markedBlobs), bigBlobsIds);
    
    clearOutput = current_binary .* ~markedBlobsMask;
    
    
    % Checking against the colour 
    whiteBlobs(:,:,1) = uint8(clearOutput) .* current_frame(:,:,1);
    whiteBlobs(:,:,2) = uint8(clearOutput) .* current_frame(:,:,2);
    whiteBlobs(:,:,3) = uint8(clearOutput) .* current_frame(:,:,3);
    
    whitesMask = rgb2gray(whiteBlobs) > 120;
    
    whitesMask = bwmorph(whitesMask, 'bridge');
    whitesMask = bwmorph(whitesMask, 'fill');
    
    whiteBlobs(:,:,1) = uint8(whitesMask) .* current_frame(:,:,1);
    whiteBlobs(:,:,2) = uint8(whitesMask) .* current_frame(:,:,2);
    whiteBlobs(:,:,3) = uint8(whitesMask) .* current_frame(:,:,3);
    
    
    % Get each white object in frame and check if it is a circle
    markedWhiteBlobs = bwconncomp(whitesMask);
    %statsWhites = regionprops(markedWhiteBlobs, 'Eccentricity');
    statsWhites = regionprops(markedWhiteBlobs, 'Perimeter', 'FilledArea');
    %blobPerimeters = [statsWhites.Perimeter];
	%blobFilledAreas = [statsWhites.FilledArea];
    
    bigWhiteBlobsIds = find(([statsWhites.Perimeter].^2 ./ (4 * pi * [statsWhites.FilledArea])) < 1.8);

	% Calculate circularities:
	%blobCircularities = blobPerimeters .^2 ./ (4 * pi * blobFilledAreas);
    
    markedWhiteBlobs = ismember(labelmatrix(markedWhiteBlobs), bigWhiteBlobsIds);
    
    clearOutputWhites = whitesMask .* markedWhiteBlobs;
    
    %clearOutputWhites = bwmorph(clearOutputWhites, 'clean');
    %clearOutputWhites = bwmorph(clearOutputWhites, 'fill');
    
    out(:,:,1) = uint8(clearOutputWhites) .* current_frame(:,:,1);
    out(:,:,2) = uint8(clearOutputWhites) .* current_frame(:,:,2);
    out(:,:,3) = uint8(clearOutputWhites) .* current_frame(:,:,3);
    
    %imshow([clearOutputWhites difference; clearOutputWhites foremn]);
    
    combine1 = xor(difference, foremn);
    combine2 = xor(clearOutputWhites, foremn);
    
    combine3 = combine1 | combine2;
    
    %imshow(combine3)
    %imshow(current_frame);
    
    %imshow([current_binary clearOutput; prev_binary prev_clearOutput]);
    previous_frame = current_frame;
    prev_binary = current_binary;
    prev_clearOutput = clearOutput;
    
    title(i);
    hold on;
    
    % Get each object in frame and it's index
    
    [labeledImage2, numberOfObjects2] = bwlabel(combine3);    
    
    % Get the properties of each object in the frame
	blobMeasurements2 = regionprops(labeledImage2, 'Perimeter', 'FilledArea', 'Centroid', 'PixelList');
    
	boundaries = bwboundaries(combine3);
	perimeters2 = [blobMeasurements2.Perimeter];
	filledAreas2 = [blobMeasurements2.FilledArea];

	% Calculate circularities:
	circularities2 = perimeters2 .^2 ./ (4 * pi * filledAreas2);
    
        
    % Go through each object in frame
	for blobNumber = 1:numberOfObjects
		% Determine the shape
		if (circularities(blobNumber) < 1.3 && circularities(blobNumber) > 0.7 && perimeters(blobNumber) > 20)
			current_xy = blobMeasurements(blobNumber).Centroid;
            mySize = blobMeasurements(blobNumber).Perimeter / pi;
            
            % Update the structure to feed in the calman filter
            counter = counter + 1;
            structure(counter).PixelList = blobMeasurements(blobNumber).PixelList;
            structure(counter).Perimeter = blobMeasurements(blobNumber).Perimeter;
            structure(counter).FilledArea = blobMeasurements(blobNumber).FilledArea;
            structure(counter).Centroid = blobMeasurements(blobNumber).Centroid;
            structure(counter).Circularity = circularities(blobNumber);
            
            ch1 = 0.0;
            ch2 = 0.0;
            ch3 = 0.0;
            
            for z = 1:size(blobMeasurements(blobNumber).PixelList, 1)
                pl = blobMeasurements(blobNumber).PixelList;
                ch1 = ch1 + double(current_frame(pl(z,2), pl(z,1), 1));
                ch2 = ch2 + double(current_frame(pl(z,2), pl(z,1), 2));
                ch3 = ch3 + double(current_frame(pl(z,2), pl(z,1), 3));
            end
            
            ch1 = double(ch1) / double(size(blobMeasurements(blobNumber).PixelList, 1));
            ch2 = double(ch2) / double(size(blobMeasurements(blobNumber).PixelList, 1));
            ch3 = double(ch3) / double(size(blobMeasurements(blobNumber).PixelList, 1));
            
            structure(counter).Channel1 = ch1;
            structure(counter).Channel2 = ch2;
            structure(counter).Channel3 = ch3;
            
            if ch1 > 200 && ch2 > 200 && ch3 > 200
                structure(counter).Colour = 'WHITE';
                overlayMessage = sprintf('WHITE');
                text(blobMeasurements(blobNumber).Centroid(1), blobMeasurements(blobNumber).Centroid(2), overlayMessage, 'Color', 'w');
                plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize, 'Color', 'y');                
            elseif ch1 > ch2 && ch1 > ch3 && ch1 > 50
                if ch2 > ch3
                    structure(counter).Colour = 'ORANGE';
                    overlayMessage = sprintf('ORANGE');
                    text(blobMeasurements(blobNumber).Centroid(1), blobMeasurements(blobNumber).Centroid(2), overlayMessage, 'Color', 'y');
                    plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize, 'Color', 'y');
                    
                else
                    structure(counter).Colour = 'PINK';
                    overlayMessage = sprintf('PINK');
                    text(blobMeasurements(blobNumber).Centroid(1), blobMeasurements(blobNumber).Centroid(2), overlayMessage, 'Color', 'm');
                    plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize, 'Color', 'y');
                end
            end
		end
    end
    
    % Go through each object in frame
    for blobNumber = 1 : numberOfObjects2
        % Determine the shape
        if (circularities2(blobNumber) < 1.4 && circularities2(blobNumber) > 0.6 && perimeters2(blobNumber) > 40)
            current_xy = blobMeasurements2(blobNumber).Centroid;
            mySize = blobMeasurements2(blobNumber).Perimeter / pi;
            
            centre = blobMeasurements2(blobNumber).Centroid;
            temp = true;
            for iter = 1:size(structure, 1)
               if abs((centre - structure(iter).Centroid).^2) < 700
                   %abs(centre(1) - structure(iter).Centroid(1)) < 70 && abs(centre(2) - structure(iter).Centroid(2)) < 70
                   temp = temp & false;
               else
                   temp = temp & true;
               end
            end
            if temp == true                   
                counter = counter + 1;
                structure(counter).PixelList = blobMeasurements2(blobNumber).PixelList;
                structure(counter).Perimeter = blobMeasurements2(blobNumber).Perimeter;
                structure(counter).FilledArea = blobMeasurements2(blobNumber).FilledArea;
                structure(counter).Centroid = blobMeasurements2(blobNumber).Centroid;
                structure(counter).Circularity = circularities2(blobNumber);

                ch1 = 0.0;
                ch2 = 0.0;
                ch3 = 0.0;

                for z = 1:size(blobMeasurements2(blobNumber).PixelList, 1)
                    pl = blobMeasurements2(blobNumber).PixelList;
                    ch1 = ch1 + double(current_frame(pl(z,2), pl(z,1), 1));
                    ch2 = ch2 + double(current_frame(pl(z,2), pl(z,1), 2));
                    ch3 = ch3 + double(current_frame(pl(z,2), pl(z,1), 3));
                end

                ch1 = double(ch1) / double(size(blobMeasurements2(blobNumber).PixelList, 1));
                ch2 = double(ch2) / double(size(blobMeasurements2(blobNumber).PixelList, 1));
                ch3 = double(ch3) / double(size(blobMeasurements2(blobNumber).PixelList, 1));

                structure(counter).Channel1 = ch1;
                structure(counter).Channel2 = ch2;
                structure(counter).Channel3 = ch3;

                if ch1 > 120 && ch2 > 120 && ch3 > 120
                    structure(counter).Colour = 'WHITE';
                    overlayMessage = sprintf('WHITE');
                    text(blobMeasurements2(blobNumber).Centroid(1), blobMeasurements2(blobNumber).Centroid(2), overlayMessage, 'Color', 'w');
                    plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);                
                elseif ch1 > ch2 && ch1 > ch3 && ch1 > 50
                    if ch2 > ch3
                        structure(counter).Colour = 'ORANGE';
                        overlayMessage = sprintf('ORANGE');
                        text(blobMeasurements2(blobNumber).Centroid(1), blobMeasurements2(blobNumber).Centroid(2), overlayMessage, 'Color', 'y');
                        plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);

                    else
                        structure(counter).Colour = 'PINK';
                        overlayMessage = sprintf('PINK');
                        text(blobMeasurements2(blobNumber).Centroid(1), blobMeasurements2(blobNumber).Centroid(2), overlayMessage, 'Color', 'm');
                        plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);
                    end
                end
            end
        end
            
            
            
            %plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);
            %plot(640 + current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);
    end
    
    
    
    
    % Anotation/Tagging
	for j = 1:num_balls
		limits(j) = numel(new_balls(j).row_of_centers);
		if nextid(j) <= limits(j)
			if new_balls(j).frame_numbers(nextid(j)) == i
				text(new_balls(j).row_of_centers(nextid(j)), new_balls(j).col_of_centers(nextid(j)), ball_name{j}, 'Clipping', 'on', 'Color', 'cyan');
				plot(new_balls(j).row_of_centers(nextid(j)), new_balls(j).col_of_centers(nextid(j)), 'g+');
                %text(640 + new_balls(j).row_of_centers(nextid(j)), new_balls(j).col_of_centers(nextid(j)), ball_name{j}, 'Clipping', 'on', 'Color', 'cyan');
                %plot(640 + new_balls(j).row_of_centers(nextid(j)), new_balls(j).col_of_centers(nextid(j)), 'g+');
				nextid(j) = nextid(j) + 1;
			end
		end
    end
    %}
    
	pause(0.5);
    
    disp(i);
    
end

disp('We are doneeee!! YEEEEY');
