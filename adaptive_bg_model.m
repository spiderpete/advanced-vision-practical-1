function [unsortedMedianR, prob_BG, history] = adaptive_bg_model(n_frame, history)
% function [unsortedMedianR, foremn2, history] = adaptive_bg_model(n_frame, history)

    % Lightnsess difference thresholds
    alpha = 0.8;
    beta = 1.2;
    p_BG = 0.99;
    p_x_FG = 0.001;
    tou = 0.05; % Decision treshold
    statNoise = 0.01;

    %similarity = ones(size(n_frame, 1), size(n_frame, 2));
    % comparison = n_frame(:,:,3);
    
    % History snapshots matching current image (on pixel bases)
    M = zeros(size(n_frame,1), size(n_frame, 2), size(history, 4));
    
    % Witty thing here (mask)
    for i = 1:size(history, 4)
        comparison = n_frame(:,:,3) ./ history(:,:,3,i);
        
        one = comparison > beta;
        two = comparison < alpha;
        
        % FG: 1s; BG: 0s
        binaryMask = one | two;
        comparison = comparison .* binaryMask;
        
        % History array organised in recency order - 1-st newest; 5-th oldest
        % FG: 0s; BG: 1s
        inverseBinaryMask = ~binaryMask;
        
        M(:,:,i) = inverseBinaryMask;
    end
    
    
    % Second part of Adaptive Change Algorithm START
    unsortedMedianR = zeros(size(history, 1), size(history, 2), size(M, 3) - 1);
    unsortedMedianG = zeros(size(history, 1), size(history, 2), size(M, 3) - 1);
    for i = 1:(size(M, 3) - 1)
        % Median of current pixel's R values from all history (in the whole picture)
        unsortedMedianR(:,:,i) = abs(history(:,:,1,i) - history(:,:,1,i + 1) + statNoise);
        
        % Median of current pixel's G values from all history (in the whole picture)
        unsortedMedianG(:,:,i) = abs(history(:,:,2,i) - history(:,:,2,i + 1) + statNoise);
        
    end
    
    sortedMedianR = sort(unsortedMedianR, 3);
    
    medianR = sortedMedianR(:,:,ceil(size(sortedMedianR, 3)/2));
    
    sortedMedianG = sort(unsortedMedianG, 3);
    medianG = sortedMedianG(:,:,ceil(size(sortedMedianG, 3)/2));

    KstdR = medianR/(0.68 * sqrt(2));
    KstdG = medianG/(0.68 * sqrt(2));
    
    % Second part of Adaptive Change Algorithm
    prob = zeros(size(M, 1), size(M, 2));
    for n = 1:size(M, 3)
        
        probR = zeros(size(n_frame, 1), size(n_frame, 2));
        probG = zeros(size(n_frame, 1), size(n_frame, 2));
        for i = 1:size(M, 1)
            for j = 1:size(M, 2)
                kernelInR = n_frame(i, j, 1) - history(i, j, 1, n);
                kernelInG = n_frame(i, j, 2) - history(i, j, 2, n);

                probR(i, j) = kernel(KstdR(i, j), kernelInR);
                probG(i, j) = kernel(KstdG(i, j), kernelInG);
               
            end
        end
        
        prob = prob + (probR .* probG);
        
        % kernelInR = n_frame(:,:,1) - history(:,:,1,n);
        % kernelInG = n_frame(:,:,2) - history(:,:,2,n);
        
    end
    
    % Probability of current frame pixels being part of the background,
    % given background images
    prob = prob * (1/size(M, 3));
    
    prob_BG = (prob * p_BG) ./ (prob * p_BG + p_x_FG * (1 - p_BG));
    prob_BG = ~(prob_BG > 0.05);
    
    %imshow(prob_BG);
    
    firstStageOutput = prob_BG .* inverseBinaryMask;
    secondStageOutput = firstStageOutput  > tou;
    
    updateHist(:,:,1) = n_frame(:,:,1) .* secondStageOutput;
    updateHist(:,:,2) = n_frame(:,:,2) .* secondStageOutput;
    updateHist(:,:,3) = n_frame(:,:,3) .* secondStageOutput;
    
    %{
    for i = size(history, 4):2
        history(:,:,3,i) = history(:,:,3,i - 1);
    end
    % Update the the most recent history snapshot
    history(:,:,3,1) = n_frame(:,:,3) .* inverseBinaryMask + history(:,:,3,1) .* binaryMask;
    %}
    for i = size(history, 4):-1:2
        history(:,:,:,i) = history(:,:,:,i - 1);
    end
    % Update the the most recent history snapshot
    % history(:,:,3,1) = n_frame(:,:, 3) .* inverseBinaryMask + history(:,:,3,1) .* binaryMask;
    % history(:,:,3,1) = updateHist + history(:,:,3,1) .* (~secondStageOutput);
    updateHistInvert(:,:,1) = history(:,:,3,1) .* (~secondStageOutput);
    updateHistInvert(:,:,2) = history(:,:,3,1) .* (~secondStageOutput);
    updateHistInvert(:,:,3) = history(:,:,3,1) .* (~secondStageOutput);
    history(:,:,:,1) = updateHist + updateHistInvert;
    % history(:,:,2,1) = n_frame(:,:, 2);
    % history(:,:,1,1) = n_frame(:,:, 1);
    
    %imshow(history(:,:,1,2) - history(:,:,1,3));
    
    foremn2 = comparison;
    % foremn2 = (M ./ 5 .* n_frame(:,:,3));