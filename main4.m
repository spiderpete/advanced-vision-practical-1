    load balls_loc.mat

    % Anotation/Tagging
    num_balls = size(new_balls,2);
    present = zeros(1, num_balls);
    limits  = zeros(1, num_balls);
    nextid  = ones(1, num_balls);
    ball_name = {'white 1', 'white 2', 'pink 1', 'pink 2', 'orange 1', 'orange 2', 'orange 3', 'orange 4', 'orange 5', 'orange 6'};
    file_name='D:\Documents - Drive (D)\Studies\Year 4\Semester 2\Advanced Vision\CourseWork\Assignment 1\Images\';
    file_format='.jpg';

    % Retain background image
    bg_file = [file_name 'bgframe.jpg'];
    background = imread(bg_file);

    n_background = normalisation(background);
    
    % Using repmat to store a structure for each frame
    N = 100;
    b = repmat(struct('structure', 1), N, 1);
    
for i = 25:87
    
    %Structure for ball tagging
    structure = struct('Colour', {}, 'Perimeter', {}, 'FilledArea', {}, 'Centroid', {}, 'Circularity', {}, 'PixelList', {}, 'Channel1', {}, 'Channel2', {}, 'Channel3', {});
    counter = 0;
    
    %Structure for ball taggingcounter
    structure2 = struct('Colour', {}, 'Perimeter', {}, 'FilledArea', {}, 'Centroid', {}, 'Circularity', {}, 'PixelList', {}, 'Channel1', {}, 'Channel2', {}, 'Channel3', {});
    counter2 = 0;
    
	filename = [file_name sprintf('%08d', i) file_format];
	current_frame=imread(filename);
    
    foremn = bg_Substraction(current_frame, background);
    
    % Adaptive background model
    n_frame = normalisation(current_frame);
    
    n_back = normalisation(background);
    
    difference = imabsdiff(n_back, n_frame);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %difference = imabsdiff(background, current_frame);
    
    difference = im2bw(difference, 0.06); % 0.06
    difference = bwareaopen(difference,250); % 250
	difference = imfill(difference, 'holes');
    
    object = bwmorph(difference, 'erode', 1); % 1
	object = ~object;
        
    % Get each object in frame and it's index
    [labeledImage, numberOfObjects] = bwlabel(~object);

	% Get the properties of each object in the frame
	blobMeasurements = regionprops(labeledImage, 'Perimeter', 'FilledArea', 'Centroid', 'PixelList');
    
	perimeters = [blobMeasurements.Perimeter];
	filledAreas = [blobMeasurements.FilledArea];

	% Calculate circularities:
	circularities = perimeters .^2 ./ (4 * pi * filledAreas);
    
    imshow(current_frame);
    hold on;

    % Image(Binary) filtering (mainly for foremn)
    foremn = bwmorph(foremn, 'clean');
    % erode -> dilate
    foremn = bwmorph(foremn, 'close');
    %foremn = imfill(foremn, 'holes');
    foremn = bwareaopen(foremn, 100);
    
    % Get each object in frame and it's indexcounter
    markedBlobs = bwconncomp(current_binary);
    stats = regionprops(markedBlobs, 'Area');
    bigBlobsIds = find([stats.Area] > 5000);
    markedBlobsMask = ismember(labelmatrix(markedBlobs), bigBlobsIds);
    
    clearOutput = current_binary .* ~markedBlobsMask;
    
    % Checking against the colour 
    whiteBlobs(:,:,1) = uint8(clearOutput) .* current_frame(:,:,1);
    whiteBlobs(:,:,2) = uint8(clearOutput) .* current_frame(:,:,2);
    whiteBlobs(:,:,3) = uint8(clearOutput) .* current_frame(:,:,3);
    
    whitesMask = rgb2gray(whiteBlobs) > 120;
    
    whitesMask = bwmorph(whitesMask, 'bridge');
    whitesMask = bwmorph(whitesMask, 'fill');
    
    whiteBlobs(:,:,1) = uint8(whitesMask) .* current_frame(:,:,1);
    whiteBlobs(:,:,2) = uint8(whitesMask) .* current_frame(:,:,2);
    whiteBlobs(:,:,3) = uint8(whitesMask) .* current_frame(:,:,3);
    
    % Get each white object in frame and check if it is a circle
    markedWhiteBlobs = bwconncomp(whitesMask);
    %statsWhites = regionprops(markedWhiteBlobs, 'Eccentricity');
    statsWhites = regionprops(markedWhiteBlobs, 'Perimeter', 'FilledArea');
    %blobPerimeters = [statsWhites.Perimeter];
	%blobFilledAreas = [statsWhites.FilledArea];
    
    bigWhiteBlobsIds = find(([statsWhites.Perimeter].^2 ./ (4 * pi * [statsWhites.FilledArea])) < 1.8);

	% Calculate circularities:
	%blobCircularities = blobPerimeters .^2 ./ (4 * pi * blobFilledAreas);
    
    markedWhiteBlobs = ismember(labelmatrix(markedWhiteBlobs), bigWhiteBlobsIds);
    
    clearOutputWhites = whitesMask .* markedWhiteBlobs;
    
    out(:,:,1) = uint8(clearOutputWhites) .* current_frame(:,:,1);
    out(:,:,2) = uint8(clearOutputWhites) .* current_frame(:,:,2);
    out(:,:,3) = uint8(clearOutputWhites) .* current_frame(:,:,3);
    
    combine1 = xor(difference, foremn);
    combine2 = xor(clearOutputWhites, foremn);
    
    combine3 = combine1 | combine2;
    
    previous_frame = current_frame;
    prev_binary = current_binary;
    prev_clearOutput = clearOutput;
    
    % Get each object in frame and it's index
    
    [labeledImage2, numberOfObjects2] = bwlabel(combine3);    
    
    % Get the properties of each object in the frame
	blobMeasurements2 = regionprops(labeledImage2, 'Perimeter', 'FilledArea', 'Centroid', 'PixelList');
    
	boundaries = bwboundaries(combine3);
	perimeters2 = [blobMeasurements2.Perimeter];
	filledAreas2 = [blobMeasurements2.FilledArea];

	% Calculate circularities:
	circularities2 = perimeters2 .^2 ./ (4 * pi * filledAreas2);
    
    % Go through each object in frame
    
    % Go through each object in frame
	for blobNumber = 1:numberOfObjects
		% Determine the shape
		if (circularities(blobNumber) < 1.3 && circularities(blobNumber) > 0.7 && perimeters(blobNumber) > 20)
			current_xy = blobMeasurements(blobNumber).Centroid;
            mySize = blobMeasurements(blobNumber).Perimeter / pi;
            
            % Update the structure to feed in the calman filter
            counter = counter + 1;
            structure(counter).PixelList = blobMeasurements(blobNumber).PixelList;
            structure(counter).Perimeter = blobMeasurements(blobNumber).Perimeter;
            structure(counter).FilledArea = blobMeasurements(blobNumber).FilledArea;
            structure(counter).Centroid = blobMeasurements(blobNumber).Centroid;
            structure(counter).Circularity = circularities(blobNumber);
            
            ch1 = 0.0;
            ch2 = 0.0;
            ch3 = 0.0;
            
            for z = 1:size(blobMeasurements(blobNumber).PixelList, 1)
                pl = blobMeasurements(blobNumber).PixelList;
                ch1 = ch1 + double(current_frame(pl(z,2), pl(z,1), 1));
                ch2 = ch2 + double(current_frame(pl(z,2), pl(z,1), 2));
                ch3 = ch3 + double(current_frame(pl(z,2), pl(z,1), 3));
            end
            
            ch1 = double(ch1) / double(size(blobMeasurements(blobNumber).PixelList, 1));
            ch2 = double(ch2) / double(size(blobMeasurements(blobNumber).PixelList, 1));
            ch3 = double(ch3) / double(size(blobMeasurements(blobNumber).PixelList, 1));
            
            structure(counter).Channel1 = ch1;
            structure(counter).Channel2 = ch2;
            structure(counter).Channel3 = ch3;
            
            if ch1 > 120 && ch2 > 120 && ch3 > 120
                structure(counter).Colour = 'white';
                overlayMessage = sprintf('white');
                text(blobMeasurements(blobNumber).Centroid(1), blobMeasurements(blobNumber).Centroid(2), overlayMessage, 'Color', 'w');
                plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);                
            elseif ch1 > ch2 && ch1 > ch3 && ch1 > 50
                if ch2 > ch3
                    structure(counter).Colour = 'orange';
                    overlayMessage = sprintf('orange');
                    text(blobMeasurements(blobNumber).Centroid(1), blobMeasurements(blobNumber).Centroid(2), overlayMessage, 'Color', 'y');
                    plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);
                else
                    structure(counter).Colour = 'pink';
                    overlayMessage = sprintf('pink');
                    text(blobMeasurements(blobNumber).Centroid(1), blobMeasurements(blobNumber).Centroid(2), overlayMessage, 'Color', 'm');
                    plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);
                end
            end
		end
    end
    
    for blobNumber = 1 : numberOfObjects2
        % Determine the shape
        if (circularities2(blobNumber) < 1.4 && circularities2(blobNumber) > 0.6 && perimeters2(blobNumber) > 40)
            current_xy = blobMeasurements2(blobNumber).Centroid;
            mySize = blobMeasurements2(blobNumber).Perimeter / pi;
            
            % Update the structure to feed in the calman filter
            counter2 = counter2 + 1;
            structure2(counter2).PixelList = blobMeasurements2(blobNumber).PixelList;
            structure2(counter2).Perimeter = blobMeasurements2(blobNumber).Perimeter;
            structure2(counter2).FilledArea = blobMeasurements2(blobNumber).FilledArea;
            structure2(counter2).Centroid = blobMeasurements2(blobNumber).Centroid;
            
            ch1 = 0.0;
            ch2 = 0.0;
            ch3 = 0.0;
            
            for z = 1:size(blobMeasurements2(blobNumber).PixelList, 1)
                pl = blobMeasurements2(blobNumber).PixelList;
                ch1 = ch1 + double(current_frame(pl(z,2), pl(z,1), 1));
                ch2 = ch2 + double(current_frame(pl(z,2), pl(z,1), 2));
                ch3 = ch3 + double(current_frame(pl(z,2), pl(z,1), 3));
            end
            
            ch1 = double(ch1) / double(size(blobMeasurements2(blobNumber).PixelList, 1));
            ch2 = double(ch2) / double(size(blobMeasurements2(blobNumber).PixelList, 1));
            ch3 = double(ch3) / double(size(blobMeasurements2(blobNumber).PixelList, 1));
            
            structure2(counter2).Channel1 = ch1;
            structure2(counter2).Channel2 = ch2;
            structure2(counter2).Channel3 = ch3;
            
            checker = 0;
            
            for q = 1:counter
                centroid = structure(q).Centroid;
                myX = centroid(1);
                myY = centroid(2);
                
                centroid2 = structure2(counter2).Centroid;
                yourX = centroid2(1);
                yourY = centroid2(2);
                
                distance = sqrt((myX - yourX)^2 + (myY - yourY)^2);
                
                if(distance < 50)
                    checker = 1;
                end
            end
            
            if(checker == 0)
                
                counter = counter + 1;
                structure(counter) = structure2(counter2);
                
                if ch1 > 120 && ch2 > 120 && ch3 > 120
                    structure2(counter2).Colour = 'white';
                    overlayMessage = sprintf('white');
                    text(blobMeasurements2(blobNumber).Centroid(1), blobMeasurements2(blobNumber).Centroid(2), overlayMessage, 'Color', 'w');
                    plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);                
                elseif ch1 > ch2 && ch1 > ch3 && ch1 > 50
                    if ch2 > ch3
                        structure2(counter2).Colour = 'orange';
                        overlayMessage = sprintf('orange');
                        text(blobMeasurements2(blobNumber).Centroid(1), blobMeasurements2(blobNumber).Centroid(2), overlayMessage, 'Color', 'y');
                        plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);
                    else
                        structure2(counter2).Colour = 'pink';
                        overlayMessage = sprintf('pink');
                        text(blobMeasurements2(blobNumber).Centroid(1), blobMeasurements2(blobNumber).Centroid(2), overlayMessage, 'Color', 'm');
                        plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);
                    end
                end
            end
        end
    end
    
    b(i).structure = structure;
    
	pause(0.2);
    
    disp(i);
    
end

% Evaluations

matches = 0;
for i = 25:86
    
     for j = 1:num_balls                                   % Iterate through all the DATA BALLS
		limits(j) = numel(new_balls(j).row_of_centers);
        if nextid(j) <= limits(j)
            if new_balls(j).frame_numbers(nextid(j)) == i
                dist = Inf;
                index = 0;
                if size(b(i).structure, 2) > 0
                    for k = 1:size(b(i).structure, 2)           % Iterate through all the DETECTED BALLS
                        distTemp = sqrt((new_balls(j).row_of_centers(nextid(j)) - b(i).structure(k).Centroid(1))^2 - (new_balls(j).col_of_centers(nextid(j)) - b(i).structure(k).Centroid(2))^2);
                        disp(i);
                        if distTemp < dist
                            dist = distTemp;
                            index = k;
                        end
                    end

                    if strcmp(ball_name{j}(1:end-2), b(i).structure(index).Colour) && (dist <= 10)
                        matches = matches + 1;
                    end
                end
                
				nextid(j) = nextid(j) + 1;
			end
        end
     end
     
end

totalToMatch = 0;
for i = 1: 10
    totalToMatch = totalToMatch + size(new_balls(i).frame_numbers, 1);
end

disp('Evaluations:');
disp(['Accuracy: ' num2str(matches * 100 / totalToMatch) ' %']);
