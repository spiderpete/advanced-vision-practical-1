    load balls_loc.mat

    % Kalman filter static initializations
    R=[[0.2845,0.0045]',[0.0045,0.0455]'];
    H=[[1,0]',[0,1]',[0,0]',[0,0]'];
    Q=0.01*eye(4);
    dt=1;
    A1=[[1,0,0,0]',[0,1,0,0]',[dt,0,1,0]',[0,0,0,0]'];  % on table, no vertical velocity
    A2=[[1,0,0,0]',[0,1,0,0]',[dt,0,1,0]',[0,dt,0,1]']; % bounce
    A3=[[1,0,0,0]',[0,1,0,0]',[dt,0,1,0]',[0,dt,0,1]']; % normal motion
    g = 6.0;              % gravity=pixels^2/time step
    Bu1 = [0,0,0,0]';   % on table, no gravity
    Bu2 = [0,0,0,g]';   % bounce
    Bu3 = [0,0,0,g]';   % normal motion
    loss=0.7;

    % multiple condensation states
    NCON=100;          % number of condensation samples
    MAXTIME=60;        % number of time frames
    
    pstop=0.05;      % probability of stopping vertical motion
    pbounce=0.30;    % probability of bouncing at current state (overestimated)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Anotation/Tagging
    num_balls = size(new_balls,2);
    present = zeros(1, num_balls);
    limits  = zeros(1, num_balls);
    nextid  = ones(1, num_balls);
    ball_name = {'white 1', 'white 2', 'pink 1', 'pink 2', 'orange 1', 'orange 2', 'orange 3', 'orange 4', 'orange 5', 'orange 6'};
    file_name='/afs/inf.ed.ac.uk/user/s11/s1132388/Desktop/practical/Images/';
    file_format='.jpg';

    % Retain background image
    bg_file = [file_name 'bgframe.jpg'];
    background = imread(bg_file);
    
    [MR,MC,Dim] = size(background);

    n_background = normalisation(background);
    
    % Using repmat to store a structure for each frame
    N = 100;
    b = repmat(struct('structure', 1), N, 1);
    
    % Using repmat to store an idStructure for each frame
    N = 100;
    idOrange = repmat(struct('idStructureOrange', 1), N, 1);
    idPink = repmat(struct('idStructurePink', 1), N, 1);
    idWhite = repmat(struct('idStructureWhite', 1), N, 1);
    
    totalNoOranges = zeros(1,100);
    totalNoPink = zeros(1, 100);
    totalNoWhite = zeros(1, 100);
    
for i = 25:87
    
    %Structure for ball tagging
    structure = struct('Colour', {}, 'Perimeter', {}, 'FilledArea', {}, 'Centroid', {}, 'Circularity', {}, 'PixelList', {}, 'Channel1', {}, 'Channel2', {}, 'Channel3', {});
    counter = 0;
    
	filename = [file_name sprintf('%08d', i) file_format];
	current_frame=imread(filename);
    Imwork = double(current_frame);
    
    foremn = bg_Substraction(current_frame, background);
    
    % Adaptive background model
    n_frame = normalisation(current_frame);
    
    n_back = normalisation(background);
    
    out2 = bg_Substraction(n_frame, n_back); % RGS
    
    out1 = foremn; % RGB
    
    difference = imabsdiff(n_back, n_frame);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %difference = imabsdiff(background, current_frame);
    
    difference = im2bw(difference, 0.06); % 0.06
    difference = bwareaopen(difference,250); % 250
	difference = imfill(difference, 'holes');
    
    object = bwmorph(difference, 'erode', 1); % 1
	object = ~object;
    
    % Get rid of noise in image
	labeled = bwlabel(object, 4);
    
    % Create image with only the edges around object
	edges = edge(labeled);
	
	% create RGB image with green edges
	edges2 = cat(3,zeros(size(edges, 1), size(edges, 2)), edges, zeros(size(edges, 1), size(edges, 2)));

    % 'Overlap' the RGB frame with the greep edges image to obtain contour around the ball
    %detectedObj = zeros(size(difference,1), size(difference, 2), 3);
	detectedObj = current_frame;
       
	for z = 1 : size(edges2,1)
        for j = 1 : size(edges2,2)
            if (edges2(z,j,2) ~= 0)
				detectedObj(z,j,1) = 0;
				detectedObj(z,j,2) = 255;
				detectedObj(z,j,3) = 0;
            end
        end
	end
        
    % Get each object in frame and it's index
    [labeledImage, numberOfObjects] = bwlabel(~object);

	% Get the properties of each object in the frame
	blobMeasurements = regionprops(labeledImage, 'Perimeter', 'FilledArea', 'Centroid', 'PixelList');
    
	boundaries = bwboundaries(~object);
	perimeters = [blobMeasurements.Perimeter];
	filledAreas = [blobMeasurements.FilledArea];

	% Calculate circularities:
	circularities = perimeters .^2 ./ (4 * pi * filledAreas);
    
    imshow(current_frame);
    hold on;
    
    % Go through each object in frame
	for blobNumber = 1:numberOfObjects
		% Determine the shape
		if (circularities(blobNumber) < 1.3 && circularities(blobNumber) > 0.7 && perimeters(blobNumber) > 20)
			current_xy = blobMeasurements(blobNumber).Centroid;
            mySize = blobMeasurements(blobNumber).Perimeter / pi;
            
            % Update the structure to feed in the calman filter
            counter = counter + 1;
            structure(counter).PixelList = blobMeasurements(blobNumber).PixelList;
            structure(counter).Perimeter = blobMeasurements(blobNumber).Perimeter;
            structure(counter).FilledArea = blobMeasurements(blobNumber).FilledArea;
            structure(counter).Centroid = blobMeasurements(blobNumber).Centroid;
            structure(counter).Circularity = circularities(blobNumber);
            
            ch1 = 0.0;
            ch2 = 0.0;
            ch3 = 0.0;
            
            for z = 1:size(blobMeasurements(blobNumber).PixelList, 1)
                pl = blobMeasurements(blobNumber).PixelList;
                ch1 = ch1 + double(current_frame(pl(z,2), pl(z,1), 1));
                ch2 = ch2 + double(current_frame(pl(z,2), pl(z,1), 2));
                ch3 = ch3 + double(current_frame(pl(z,2), pl(z,1), 3));
            end
            
            ch1 = double(ch1) / double(size(blobMeasurements(blobNumber).PixelList, 1));
            ch2 = double(ch2) / double(size(blobMeasurements(blobNumber).PixelList, 1));
            ch3 = double(ch3) / double(size(blobMeasurements(blobNumber).PixelList, 1));
            
            structure(counter).Channel1 = ch1;
            structure(counter).Channel2 = ch2;
            structure(counter).Channel3 = ch3;
            
            if ch1 > 200 && ch2 > 200 && ch3 > 200
                structure(counter).Colour = 'WHITE';
                overlayMessage = sprintf('WHITE');
                text(blobMeasurements(blobNumber).Centroid(1), blobMeasurements(blobNumber).Centroid(2), overlayMessage, 'Color', 'w');
                plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);                
            elseif ch1 > ch2 && ch1 > ch3 && ch1 > 50
                if ch2 > ch3
                    structure(counter).Colour = 'ORANGE';
                    overlayMessage = sprintf('ORANGE');
                    text(blobMeasurements(blobNumber).Centroid(1), blobMeasurements(blobNumber).Centroid(2), overlayMessage, 'Color', 'y');
                    plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);
                    
                else
                    structure(counter).Colour = 'PINK';
                    overlayMessage = sprintf('PINK');
                    text(blobMeasurements(blobNumber).Centroid(1), blobMeasurements(blobNumber).Centroid(2), overlayMessage, 'Color', 'm');
                    plot(current_xy(1), current_xy(2), 'o', 'MarkerSize', mySize);
                end
            end
		end
    end
    
    b(i).structure = structure;
    
    
    
    
    
    
    
    
    
    
    
    
    
	pause(0.2);
    
    disp(i);
    
end

disp('We are done!');
